package com.restaurant.mapper;

import com.restaurant.dto.BillDTO;
import com.restaurant.entity.Bill;
import com.restaurant.entity.BillItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Mapper class for bill
 */
@Component
public class BillMapper implements GenericMapper<BillDTO, Bill>{
    /**
     * Bill items mapper
     */
    @Autowired
    BillItemMapper billItemMapper;
    /**
     * Convert Bill entity to bill dto
     *
     * @param bill
     * @return
     */
    @Override
    public BillDTO entityToDTO(Bill bill) {
        BillDTO billDTO = new BillDTO();
        billDTO.setId(bill.getId());
        billDTO.setTotal(bill.getTotalPrice());
        billDTO.setDate(bill.getOrderDate());
        Set<BillItem> billItems = bill.getBillItems();

        Map<Integer, Integer> finalMap = new HashMap<>();
        for (Iterator<BillItem> it = billItems.iterator(); it.hasNext(); ) {
            BillItem f = it.next();
            finalMap.put(f.getMenu().getId(), f.getQuantity());
        }
        billDTO.setMenuAndQuantity(finalMap);

        billDTO.setBillItemDTOS(billItemMapper.entityToDTO(billItems));
        return billDTO;
    }

    /**
     * Convert List bill to list bill dto
     *
     * @param billList
     * @return
     */
    @Override
    public List<BillDTO> entityToDTO(List<Bill> billList) {
        return billList.stream().map(
                        this::entityToDTO)
                .collect(Collectors.toList());
    }

    /**
     * Convert set of bill to set of bill dto
     * @param billSet
     * @return
     */
    @Override
    public Set<BillDTO> entityToDTO(Set<Bill> billSet) {
        return billSet.stream().map(
                        this::entityToDTO)
                .collect(Collectors.toSet());
    }

    /**
     * Convert Page<Menu> to Page<MenuDTO>
     *
     * @param billPage
     * @return
     */
    @Override
    public Page<BillDTO> entityToDTO(Page<Bill> billPage) {
        List<Bill> billList = billPage.getContent();
        List<BillDTO> billDTOList = entityToDTO(billList);
        return new PageImpl<BillDTO>(billDTOList, billPage.getPageable(), billDTOList.size());
    }

    /**
     * Convert DTO to Bill Entity
     *
     * @param billDTO
     * @return
     */
    @Override
    public Bill dtoToEntity(BillDTO billDTO) {
        Bill bill = new Bill();
        bill.setOrderDate(billDTO.getDate());
        bill.setTotalPrice(billDTO.getTotal());
        bill.setId(billDTO.getId());
        return bill;
    }

    /**
     * Convert List DTOs to List Bill entities
     *
     * @param billDTOList
     * @return
     */
    @Override
    public List<Bill> dtoToEntity(List<BillDTO> billDTOList) {
        return billDTOList.stream().map(
                        this::dtoToEntity)
                .collect(Collectors.toList());
    }
}
