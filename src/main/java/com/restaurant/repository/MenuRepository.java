package com.restaurant.repository;

import com.restaurant.entity.Menu;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import java.util.Optional;

/**
 * Menu repository
 */
@Repository
public interface MenuRepository extends JpaRepository<Menu, Integer> {
    @Query(value = "select * from Menu where food_drink_name like ?1", nativeQuery = true)
    Optional<Menu> findMenuByName(String name);
}
