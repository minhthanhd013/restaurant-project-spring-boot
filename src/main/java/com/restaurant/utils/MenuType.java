package com.restaurant.utils;

/**
 * Enumerate class to store menu type
 */
public enum MenuType {
    BREAKFAST, LUNCH, DINNER, SOFT, ALCOHOL
}
