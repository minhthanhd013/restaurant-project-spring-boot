package com.restaurant.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.restaurant.utils.Constant;

import javax.persistence.*;
import java.util.Set;

/**
 * Bill entity
 */
@Entity
@Table(name = Constant.ENTITY_BILL)
public class Bill {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = Constant.ENTITY_BILL_ID)
    private int id;
    @Column(name = Constant.ENTITY_BILL_ORDER_DATE)
    private String orderDate;
    @Column(name = Constant.ENTITY_BILL_TOTAL_PRICE)
    private double totalPrice;
    @OneToMany(mappedBy = "bill", cascade = {CascadeType.REMOVE, CascadeType.PERSIST})
    @JsonManagedReference
    Set<BillItem> billItems;

    /**
     * Constructor with arg
     */
    public Bill(int id, String orderDate, double totalPrice) {
        this.id = id;
        this.orderDate = orderDate;
        this.totalPrice = totalPrice;

    }

    /**
     * Constructor with no-arg
     */
    public Bill() {
    }

    /**
     * Get total price
     *
     * @return
     */
    public double getTotalPrice() {
        return totalPrice;
    }

    /**
     * Set total price
     *
     * @param totalPrice
     */
    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    /**
     * Get bill id
     *
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     * Set bill id
     *
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Get order date
     *
     * @return
     */
    public String getOrderDate() {
        return orderDate;
    }

    /**
     * set order date
     *
     * @param orderDate
     */
    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    /**
     * get list of bill items
     *
     * @return
     */
    public Set<BillItem> getBillItems() {
        return billItems;
    }

    /**
     * Set bill items
     *
     * @param billItems
     */
    public void setBillItems(Set<BillItem> billItems) {
        this.billItems = billItems;
    }
}
