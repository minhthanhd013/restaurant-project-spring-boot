package com.restaurant.service;

import com.restaurant.dto.BillItemDTO;
/**
 * Bill items service extended from GenericService => Easy to add new method specifically use for bill items
 */
public interface BillItemService extends GenericService<BillItemDTO> {
}
