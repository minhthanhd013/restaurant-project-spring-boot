package com.restaurant.mapper;

import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Set;

/**
 * Generic Mapper for Bill, Bill item, menu
 * @param <T>
 * @param <U>
 */
public interface GenericMapper<T, U> {
    /**
     * Convert Entity to DTO
     * @param u
     * @return
     */
    T entityToDTO(U u);

    /**
     * Convert List of U to list of T
     * @param uList
     * @return
     */
    List<T> entityToDTO(List<U> uList);

    /**
     * Convert set entity to set dto
     * @param uSet
     * @return
     */
    Set<T> entityToDTO(Set<U> uSet);

    /**
     * Convert page entity to page dto
     * @param uPage
     * @return
     */
    Page<T> entityToDTO(Page<U> uPage);

    /**
     * Convert dto to entity
     * @param t
     * @return
     */
    U dtoToEntity(T t);

    /**
     * Convert List dto to List entity
     * @param tList
     * @return
     */
    List<U> dtoToEntity(List<T> tList);


}
