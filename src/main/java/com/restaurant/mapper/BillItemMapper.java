package com.restaurant.mapper;

import com.restaurant.dto.BillItemDTO;
import com.restaurant.entity.BillItem;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Mapper class for bill items
 */
@Component
public class BillItemMapper implements GenericMapper<BillItemDTO, BillItem>{
    /**
     * Convert bill items entity to DTO .
     *
     * @param billItem
     * @return
     */
    public BillItemDTO entityToDTO(BillItem billItem) {
        BillItemDTO billItemDTO = new BillItemDTO();
        billItemDTO.setBillId(billItem.getBill().getId());
        billItemDTO.setMenu(billItem.getMenu());
        billItemDTO.setQuantity(billItem.getQuantity());
        billItemDTO.setId(billItem.getId());

        return billItemDTO;
    }

    /**
     * Convert List bill items to list bill items dto
     *
     * @param billItemList
     * @return
     */
    public List<BillItemDTO> entityToDTO(List<BillItem> billItemList) {
        return billItemList.stream().map(
                        this::entityToDTO)
                .collect(Collectors.toList());
    }

    /**
     * Convert Set of bill items to set bill items DTO
     * @param billItemSet
     * @return
     */
    public Set<BillItemDTO> entityToDTO(Set<BillItem> billItemSet) {
        return billItemSet.stream().map(
                        this::entityToDTO)
                .collect(Collectors.toSet());
    }
    /**
     * Convert Page<BillItem> to Page<BillItem>
     *
     * @param billItemsPage
     * @return
     */
    public Page<BillItemDTO> entityToDTO(Page<BillItem> billItemsPage) {
        List<BillItem> billItemList = billItemsPage.getContent();
        List<BillItemDTO> billItemDTOList = entityToDTO(billItemList);
        return new PageImpl<>(billItemDTOList, billItemsPage.getPageable(), billItemDTOList.size());
    }

    /**
     * Convert DTO to Bill items Entity
     *
     * @param billItemDTO
     * @return
     */
    public BillItem dtoToEntity(BillItemDTO billItemDTO) {
        BillItem billItem = new BillItem();
        billItem.setMenu(billItemDTO.getMenu());
        billItem.setQuantity(billItemDTO.getQuantity());
        billItem.setId(billItemDTO.getId());

        return billItem;
    }

    /**
     * Convert List DTOs to List Bill items entities
     *
     * @param billItemDTOList
     * @return
     */
    public List<BillItem> dtoToEntity(List<BillItemDTO> billItemDTOList) {
        return billItemDTOList.stream().map(
                        this::dtoToEntity)
                .collect(Collectors.toList());
    }
}
